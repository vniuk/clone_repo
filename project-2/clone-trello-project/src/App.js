import React from "react";
import {registration} from "./request/registration";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <button onClick={registration}>fetch</button>
      </header>
    </div>
  );
}

export default App;
